Number.prototype.formatCurrency = function()
{
  var
    result = (Math.round(this*100)/100).toString(),
    pos = result.indexOf('.');
  if (pos<0) result += '.00'; else if (result.length-pos<3) result += '0';
  return result;
};

Number.prototype.in = function(rangeStart,rangeEnd)
{
  return this>=rangeStart && this<=rangeEnd;
};

var currencies = null;

function optionHTML(name,charCode,nominal,value)
{
  return '<option style="background-image: url(\'flags/' + charCode + '.png\')" value="' + charCode + '" data-nominal="' + nominal + '" data-value="' + value + '">' + name + '</option>';
}

$.get('http://university.netology.ru/api/currency',{},function(response)
{
  currencies = response;
  $('form').removeClass('loading');
  $(currencies).each(function()
  {
    $(optionHTML(this.Name,this.CharCode,this.Nominal,this.Value)).appendTo('select');
  });
  $('option[value="PLN"]').after(optionHTML('Российский рубль','RUR',1,1.0));
  $('#currency-source').val('RUR').change();
  $('#currency-destination').val('USD').change();
});

var
  selectedCurrencies = {from: '', to: ''};

function exchangeInputValues()
{
  var src = $('#value-source'), dst = $('#value-destination'), temp = src.val();
  src.val(dst.val());
  dst.val(temp);
}

$('select').on('change',function()
{
  if ($(this).attr('id')=='currency-source' && $(this).val()==$('#currency-destination').val())
  {
    $('#currency-destination').val(selectedCurrencies.from).change();
    exchangeInputValues();
  } else if ($(this).attr('id')=='currency-destination' && $(this).val()==$('#currency-source').val())
  {
    $('#currency-source').val(selectedCurrencies.to).change();
    exchangeInputValues();
  } else {
    convert();
  }
  $(this).css('background-image', 'url(\'flags/' + $(this).val() + '.png\')');
  $(this).next().html($(this).val());
  if ($(this).attr('id')=='currency-source') selectedCurrencies.from = $(this).val(); else selectedCurrencies.to = $(this).val();
});

$('input').on('input propertychange',function()
{
  convert(this.id!='value-source');
});

$('input').on('keypress',function(event)
{
  var c = event.keyCode;
  if (c==0) c = event.which;
  var
    caretPos = this.selectionStart,
    dotPos = this.value.indexOf('.'),
    minusPos = this.value.indexOf('-'),
    empty = this.value=='';
  if (!(c==8 || c.in(37,40) || c==45 && !caretPos>0 && minusPos<0 && !empty || c==46 && dotPos<0 && caretPos>minusPos && !empty || c.in(48,57))) event.preventDefault();
});

function convert(reverse)
{
  reverse = arguments.length<1 ? false : reverse;
  var
    selectSource = $('#currency-source'),
    selectDestination = $('#currency-destination'),
    inputSource = $('#value-source'),
    inputDestination = $('#value-destination');
  var
    src = reverse ? inputDestination.val() : inputSource.val(),
    ns = $('option[value='+(reverse ? selectDestination : selectSource).val()+']').data('nominal'),
    nd = $('option[value='+(reverse ? selectSource : selectDestination).val()+']').data('nominal'),
    vs = $('option[value='+(reverse ? selectDestination : selectSource).val()+']').data('value'),
    vd = $('option[value='+(reverse ? selectSource : selectDestination).val()+']').data('value'),
    dst = src / ns * vs / vd * nd;
  (reverse ? inputSource : inputDestination).val(dst.formatCurrency());
}